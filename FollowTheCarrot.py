from Robot import *
from Path import *
from ShowPath import *
import math
from math import pi

carrotX = 0
carrotY = 0

# load a path file
p = Path("exam2020.json")
path = p.getPath()

# plot the path
sp = ShowPath(path)


# make a robot to move around
robot = Robot()
marker=len(path)


#Calculating the Distance between two points
def euclidianDistance(X1,X2, Y1,Y2):
	xDis = ((X1 - X2)**2)
	yDis = ((Y1 - Y2)**2)
	return math.sqrt(xDis + yDis)




def findClosestPointonPathtoRobot():
	maxDis = 9999999
	#iterate over all points to find the closest Point on Path from the Robots Position
	if(len(path)<6):
		robot.setMotion(0, 0)
		return

	for i in range(0,len(path)):
		distancePathRobot = euclidianDistance(path[i]['X'],posRobot['X'],   path[i]['Y'],posRobot['Y'])
		if distancePathRobot < maxDis : 
			maxDis = distancePathRobot
			closestPointX = posRobot['X']
			closestPointY = posRobot['Y']
	return closestPointX,closestPointY


def findCarrotPoint(lookaheadDis, closeX,closeY, marker):
	#TODO: store last carrotpoint
	#print("frist marker: ",marker," ",len(path))
	if marker > len(path)/2 and euclidianDistance( path[len(path)-1]['X'],posRobot['X'],           path[len(path)-1]['Y'],posRobot['Y'])<(lookaheadDis+0.2):
		#print("CloseToFinish")
		return path[len(path)-1]['X'],path[len(path)-1]['Y'],marker
	carrotX= 0
	carrotY= 0
	#x = lookaheadDis+1
	k = marker
	#iterate over all points on path and find the one closest to the lookahead distance
	#optimize? only iterate until the lookaheaddistance is reached IF the path coordinates are ordered.
	
	for i in range(k,len(path)):
		x = euclidianDistance(closeX,path[i]['X'],      closeY,path[i]['Y']) 
		if  x > lookaheadDis:
			carrotX = path[i]['X']
			carrotY = path[i]['Y']
			k = i
			break;
	#print(marker," ",i," ",len(path))
	return carrotX,carrotY,k




lookaheadDis = 0.78
marker = 0
# move the robot
#robot.setMotion(0.2, 0.2)

for i in range(100000000000):
	#time.sleep(0.1)
	posRobot = robot.getPosition()

	#Stops if robot is at finish 
	distanceToFinish = euclidianDistance(path[len(path)-1]['X'],posRobot['X'],       path[len(path)-1]['Y'],posRobot['Y'])
	if(distanceToFinish<= 0.15):
		break

	closeX,closeY = findClosestPointonPathtoRobot()

	carrotX = 0
	carrotY = 0
	#print("marker before: ", marker)
	carrotX,carrotY,marker = findCarrotPoint(lookaheadDis,closeX,closeY,marker)


	angleError = ( math.atan2((carrotY-posRobot['Y']),carrotX-posRobot['X'])- robot.getHeading() )
	#print(angleError)

	otherWayAround = (2*pi - abs(angleError))
	if angleError >= 0:
		otherWayAround *= -1
	if abs(angleError) >= abs(otherWayAround):
		angleError = otherWayAround

	robot.setMotion(500,angleError*3)
	# Plot the current position and the look-ahead point:
	sp.update(robot.getPosition(), [carrotX,carrotY])

#echoes = robot.getLaser()
#print(echoes)
robot.setMotion(0, 0)
